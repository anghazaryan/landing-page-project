const PRODUCT_CARDS = "products__grid";
const OFFER_CARDS = "offer-products__offer-cards";

const products_card = [
  {
    label: "Vegetable",
    caption: "Calabrese Broccoli",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-1.jpeg",
  },
  {
    label: "Vegetable",
    caption: "Fresh Corn",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-2.jpeg",
  },
  {
    label: "Millets",
    caption: "Dried Pistachio",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-3.jpeg",
  },
  {
    label: "Vegetable",
    caption: "Vegan Red Tomato",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-4.jpeg",
  },
  {
    label: "Millets",
    caption: "Organic Almonds",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-5.jpeg",
  },
  {
    label: "Millets",
    caption: "Brown Hazelnut",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-6.jpeg",
  },
];

const offer_card = [
  {
    label: "Vegetable",
    caption: "Vegan Red Tomato",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-4.jpeg",
  },
  {
    label: "Millets",
    caption: "Organic Almonds",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-5.jpeg",
  },
  {
    label: "Vegetable",
    caption: "Fresh Corn",
    price: 25.0,
    discountedPrice: 21.0,
    stars: 5,
    imagePath: "img/prod-2.jpeg",
  },
];

function generateProductCard(product) {
  const card = document.createElement("div");
  card.className = "product-card";

  card.innerHTML = `
        <a href="#" class="product-card__btn">${product.label}</a>
        <figure class="product-card__figure">
          <img src="${product.imagePath}" alt="Photo of ${
    product.caption
  }" class="product-card__img" />
          <figcaption class="product-card__caption">${
            product.caption
          }</figcaption>
        </figure>
        <div class="price-and-stars">
          <p class="price-and-stars__price">$ ${product.price.toFixed(
            2
          )} USD</p>
          <p class="price-and-stars__discounted-price">$ ${product.discountedPrice.toFixed(
            2
          )} USD</p>
          <div class="stars">
            ${Array.from(
              { length: product.stars },
              () =>
                "<svg class='stars__icon'><use xlink:href='img/sprite.svg#icon-star'></use></svg>"
            ).join("")}
          </div>
        </div>
    `;

  return card;
}

function renderProductCards(classname, products) {
  const productContainer = document.getElementsByClassName(classname);

  products.forEach((product) => {
    const productCardHTML = generateProductCard(product);
    productContainer[0].appendChild(productCardHTML);
  });
}

renderProductCards(PRODUCT_CARDS, products_card);
renderProductCards(OFFER_CARDS, offer_card);
