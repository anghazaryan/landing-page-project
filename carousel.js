function carousel() {
    const prevBtn = document.getElementById("prev-btn");
    const nextBtn = document.getElementById("next-btn");
    const img = document.getElementById("carousel-img");

    const img_path = "img/prod-";
    const img_length = 6;
    let index = 1;

    prevBtn.addEventListener("click", () => {
        if (index <= 1)
            index = img_length + 1;

        img.src = `${img_path}${--index}.jpeg`;

        console.log(`prev - ${img_path}${index}.jpeg`)

    });

    nextBtn.addEventListener("click", () => {
        if (index >= img_length) 
            index = 0;

        img.src = `${img_path}${++index}.jpeg`;

        console.log(`next - ${img_path}${index}.jpeg`)
    })
}

carousel();